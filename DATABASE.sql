/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.1.39-MariaDB : Database - laracom
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `failed_jobs` */

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2019_08_19_000000_create_failed_jobs_table',1),
(2,'2020_04_18_101953_create_products_table',1),
(3,'2020_04_18_132841_create_profiles_table',1),
(4,'2020_04_21_154729_create_stocks_table',1),
(5,'2020_04_24_084350_create_orders_table',1),
(6,'2020_04_26_123151_create_reminders_table',1),
(7,'2020_04_27_044831_create_newsletters_table',1);

/*Table structure for table `newsletters` */

DROP TABLE IF EXISTS `newsletters`;

CREATE TABLE `newsletters` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `newsletters` */

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cart` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phonenumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipcode` int(11) NOT NULL,
  `payment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `orders` */

insert  into `orders`(`id`,`created_at`,`updated_at`,`user_id`,`name`,`cart`,`phonenumber`,`country`,`city`,`address`,`zipcode`,`payment_id`) values 
(1,'2021-10-08 12:17:28','2021-10-08 12:17:28',5,'Fanny','O:8:\"App\\Cart\":3:{s:5:\"items\";a:1:{i:0;a:5:{s:8:\"quantity\";i:1;s:5:\"price\";i:20;s:4:\"size\";s:1:\"L\";s:4:\"item\";O:11:\"App\\Product\":27:{s:13:\"\0*\0connection\";s:5:\"mysql\";s:8:\"\0*\0table\";s:8:\"products\";s:13:\"\0*\0primaryKey\";s:2:\"id\";s:10:\"\0*\0keyType\";s:3:\"int\";s:12:\"incrementing\";b:1;s:7:\"\0*\0with\";a:0:{}s:12:\"\0*\0withCount\";a:0:{}s:10:\"\0*\0perPage\";i:15;s:6:\"exists\";b:1;s:18:\"wasRecentlyCreated\";b:0;s:13:\"\0*\0attributes\";a:10:{s:2:\"id\";i:19;s:4:\"name\";s:5:\"Cloth\";s:5:\"brand\";s:8:\"Skechers\";s:5:\"price\";i:20;s:5:\"image\";s:53:\"products/wEkRw9RgLGDI98d71Sb1g1HfCALLcUB1B1fqmkdC.jpg\";s:6:\"gender\";s:4:\"Male\";s:8:\"category\";s:5:\"Shoes\";s:8:\"quantity\";i:1;s:10:\"created_at\";s:19:\"2021-10-08 12:14:29\";s:10:\"updated_at\";s:19:\"2021-10-08 12:14:29\";}s:11:\"\0*\0original\";a:10:{s:2:\"id\";i:19;s:4:\"name\";s:5:\"Cloth\";s:5:\"brand\";s:8:\"Skechers\";s:5:\"price\";i:20;s:5:\"image\";s:53:\"products/wEkRw9RgLGDI98d71Sb1g1HfCALLcUB1B1fqmkdC.jpg\";s:6:\"gender\";s:4:\"Male\";s:8:\"category\";s:5:\"Shoes\";s:8:\"quantity\";i:1;s:10:\"created_at\";s:19:\"2021-10-08 12:14:29\";s:10:\"updated_at\";s:19:\"2021-10-08 12:14:29\";}s:10:\"\0*\0changes\";a:0:{}s:8:\"\0*\0casts\";a:0:{}s:17:\"\0*\0classCastCache\";a:0:{}s:8:\"\0*\0dates\";a:0:{}s:13:\"\0*\0dateFormat\";N;s:10:\"\0*\0appends\";a:0:{}s:19:\"\0*\0dispatchesEvents\";a:0:{}s:14:\"\0*\0observables\";a:0:{}s:12:\"\0*\0relations\";a:0:{}s:10:\"\0*\0touches\";a:0:{}s:10:\"timestamps\";b:1;s:9:\"\0*\0hidden\";a:0:{}s:10:\"\0*\0visible\";a:0:{}s:11:\"\0*\0fillable\";a:0:{}s:10:\"\0*\0guarded\";a:1:{i:0;s:1:\"*\";}}s:10:\"product_id\";i:19;}}s:13:\"totalQuantity\";i:1;s:10:\"totalPrice\";i:20;}','0823338206740','Indonesia','Kediri','jl tegalan',64182,NULL);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `products` */

insert  into `products`(`id`,`name`,`brand`,`price`,`image`,`gender`,`category`,`quantity`,`created_at`,`updated_at`) values 
(1,'AIR JORDAN 1 X OFF-WHITE NRG \"OFF WHITE UNC\"','Nike',1375,'products/1.jpg','Female','Shoes',1,NULL,NULL),
(2,'STUSSY X AIR ZOOM SPIRIDON CAGED \"PURE PLATINUM\"','Nike',225,'products/2.jpg','Unisex','Shoes',12,NULL,NULL),
(3,'SUPREME X AIR FORCE 1 LOW \"BOX LOGO - WHITE\"','Nike',275,'products/3.jpg','Male','Shoes',1,NULL,NULL),
(4,'SACAI X LDV WAFFLE \"BLACK NYLON\"','Nike',190,'products/4.jpg','Male','Shoes',1,NULL,NULL),
(5,'AIR JORDAN 1 RETRO HIGH \"SHATTERED BACKBOARD\"','Nike',980,'products/5.jpg','Male','Shoes',14,NULL,NULL),
(6,'YEEZY BOOST 350 V2 \"CREAM\"','Adidas',780,'products/6.jpg','Unisex','Shoes',3,NULL,NULL),
(7,'YEEZY BOOST 350 V2\"YECHEIL NON-REFLECT\"','Adidas',978,'products/7.jpg','Male','Shoes',5,NULL,NULL),
(8,'YEEZY BOOST 350 V2 \"FROZEN YELLOW\"','Adidas',1100,'products/8.jpg','Unisex','Shoes',3,NULL,NULL),
(9,'AIR JORDAN 5 RETRO SP \"MUSLIN\"','Nike',1499,'products/9.jpg','Male','Shoes',3,NULL,NULL),
(10,'AIR JORDAN 1 RETRO HIGH ZOOM \"RACER BLUE\"','Nike',625,'products/10.jpg','Male','Shoes',5,NULL,NULL),
(11,'FENTY SLIDE \"PINK BOW \"','Puma',399,'products/11.jpg','Female','Shoes',3,NULL,NULL),
(12,'WMNS RS-X TRACKS \"FAIR AQUA\"','Puma',499,'products/12.jpg','Female','Shoes',3,NULL,NULL),
(13,'OLD SKOOL \'BLACK WHITE\' \"BLACK WHITE\"','Vans',239,'products/13.jpg','Unisex','Shoes',6,NULL,NULL),
(14,'OLD SKOOL \"YACHT CLUB\"','Vans',359,'products/14.jpg','Unisex','Shoes',5,NULL,NULL),
(15,'VANS OLD SKOOL \"RED CHECKERBOARD \"','Vans',419,'products/15.jpg','Unisex','Shoes',5,NULL,NULL),
(16,'ALL STAR 70S HI \"MILK\"','Converse',579,'products/16.jpg','Unisex','Shoes',5,NULL,NULL),
(17,'ALL-STAR 70S HI \"PLAY\"','Puma',619,'products/17.jpg','Unisex','Shoes',3,NULL,NULL),
(18,'FEAR OF GOD CHUCK 70 HI \"NATURAL\"','Converse',1259,'products/18.jpg','Female','Shoes',5,NULL,NULL),
(19,'Cloth','Skechers',20,'products/wEkRw9RgLGDI98d71Sb1g1HfCALLcUB1B1fqmkdC.jpg','Male','Shoes',1,'2021-10-08 12:14:29','2021-10-08 12:14:29');

/*Table structure for table `profiles` */

DROP TABLE IF EXISTS `profiles`;

CREATE TABLE `profiles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `phonenumber` bigint(20) DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `zipcode` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profiles_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `profiles` */

insert  into `profiles`(`id`,`user_id`,`phonenumber`,`country`,`city`,`address`,`zipcode`,`created_at`,`updated_at`) values 
(1,1,11151552928,'Singapore','Singapore','Buangkok Green 512-4a',42132,NULL,NULL),
(2,2,8215551234,'Indonesia','Medan','Danau Toba',27321,NULL,NULL),
(3,3,42912345,'United State of America','Seattle','Downtown Seattle ST 17',78231,NULL,NULL),
(4,4,32912345,'China','Guangzhou','ST 23a',78213,NULL,NULL),
(5,5,NULL,NULL,NULL,NULL,NULL,'2021-10-08 12:16:20','2021-10-08 12:16:20');

/*Table structure for table `reminders` */

DROP TABLE IF EXISTS `reminders`;

CREATE TABLE `reminders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reminder` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `reminders` */

insert  into `reminders`(`id`,`reminder`,`created_at`,`updated_at`) values 
(1,'Something',NULL,'2021-10-08 12:13:58');

/*Table structure for table `stocks` */

DROP TABLE IF EXISTS `stocks`;

CREATE TABLE `stocks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `stocks_product_id_index` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `stocks` */

insert  into `stocks`(`id`,`product_id`,`name`,`quantity`,`created_at`,`updated_at`) values 
(1,1,'32',0,NULL,NULL),
(2,1,'33',10,NULL,NULL),
(3,1,'34',10,NULL,NULL),
(4,1,'35',10,NULL,NULL),
(5,1,'36',10,NULL,NULL),
(6,1,'37',10,NULL,NULL),
(7,1,'38',10,NULL,NULL),
(8,2,'39',10,NULL,NULL),
(9,2,'40',10,NULL,NULL),
(10,2,'32',0,NULL,NULL),
(11,2,'33',10,NULL,NULL),
(12,2,'34',10,NULL,NULL),
(13,2,'35',10,NULL,NULL),
(14,3,'36',10,NULL,NULL),
(15,3,'37',10,NULL,NULL),
(16,3,'38',10,NULL,NULL),
(17,3,'39',10,NULL,NULL),
(18,3,'40',10,NULL,NULL),
(19,4,'32',0,NULL,NULL),
(20,4,'33',10,NULL,NULL),
(21,4,'34',10,NULL,NULL),
(22,4,'35',10,NULL,NULL),
(23,4,'36',10,NULL,NULL),
(24,4,'37',10,NULL,NULL),
(25,4,'38',10,NULL,NULL),
(26,5,'39',10,NULL,NULL),
(27,5,'40',10,NULL,NULL),
(28,5,'32',0,NULL,NULL),
(29,5,'33',10,NULL,NULL),
(30,6,'34',10,NULL,NULL),
(31,6,'35',10,NULL,NULL),
(32,6,'36',10,NULL,NULL),
(33,6,'37',10,NULL,NULL),
(34,7,'38',10,NULL,NULL),
(35,7,'39',10,NULL,NULL),
(36,7,'40',10,NULL,NULL),
(37,7,'32',0,NULL,NULL),
(38,7,'33',10,NULL,NULL),
(39,7,'34',10,NULL,NULL),
(40,8,'35',10,NULL,NULL),
(41,8,'36',10,NULL,NULL),
(42,8,'37',10,NULL,NULL),
(43,8,'38',10,NULL,NULL),
(44,9,'39',10,NULL,NULL),
(45,9,'40',10,NULL,NULL),
(46,9,'32',0,NULL,NULL),
(47,10,'33',10,NULL,NULL),
(48,11,'34',10,NULL,NULL),
(49,12,'35',10,NULL,NULL),
(50,13,'36',10,NULL,NULL),
(51,14,'37',10,NULL,NULL),
(52,15,'38',10,NULL,NULL),
(53,16,'39',10,NULL,NULL),
(54,17,'39',10,NULL,NULL),
(55,17,'40',10,NULL,NULL),
(56,18,'38',10,NULL,NULL),
(57,18,'39',10,NULL,NULL),
(58,19,'L',99,'2021-10-08 12:15:02','2021-10-08 12:17:28'),
(59,19,'M',100,'2021-10-08 12:15:17','2021-10-08 12:15:17'),
(60,19,'XL',100,'2021-10-08 12:15:29','2021-10-08 12:15:29');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Customer',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`role`,`remember_token`,`created_at`,`updated_at`) values 
(1,'Admin','admin@therack.com',NULL,'$2y$10$kOJuUf82Asj9shoUB01LAOXsGNb1spYJ94BvGT3T8UoTMZbndY/kS','Admin',NULL,NULL,NULL),
(2,'Dawn Roe','dawnroe@gmail.com',NULL,'$2y$10$uMsbwljMgGwruWGqFT/7qOua7vNpBUQGT1rgXduxj39eOkf3/JyFu','Customer',NULL,NULL,NULL),
(3,'John Doe','john_doe@gmail.com',NULL,'$2y$10$jjDA5fZlMuS0CZ1JcAzOaum/1q6rTstINO2pUvNr.sZ85/lwyHjx6','Customer',NULL,NULL,NULL),
(4,'Emillie Norton','emillie_norton@gmail.com',NULL,'$2y$10$MQSsC26.9qYk4Zieu8gYI.OV2Vz525FPy73kQAwNHTm6Bfl5ord0q','Customer',NULL,NULL,NULL),
(5,'Fanny','fannybagus9f@gmail.com',NULL,'$2y$10$WfkTrE9qr2GShqoV7gk9xu4cI.Txqrme0XA.5c5Yg.kBPxMb0qTJ2','Customer',NULL,'2021-10-08 12:16:20','2021-10-08 12:16:20');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
